using UnityEngine;

namespace Utility.Graphics
{
    public class KeepUpright : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            SetRotation();
        }

        // Get rotation of parent and rotate to keep upright       
        private void SetRotation()
        {
            float heading = gameObject.transform.parent.rotation.eulerAngles.y;

            transform.rotation = Quaternion.Euler(
                transform.rotation.eulerAngles.x,
                -1.0f * heading, // THIS COUNTER-ROTATES BY DOUBLE??
                transform.rotation.eulerAngles.z
            );
        }
    }
}