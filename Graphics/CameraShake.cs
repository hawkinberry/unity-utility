﻿using System.Collections;
using UnityEngine;

namespace Utility.Graphics
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] private float m_DefaultDuration = .15f;
        [SerializeField] private float m_DefaultMagnitude = .4f;

        [SerializeField] private bool localPosition = true;

        private static CameraShake _instance;
        private Coroutine shakeCoroutineReference;

        private void Start()
        {
            if (_instance == null)
            {
                _instance = this;
            }
        }

        public static void Shake()
        {
            _instance.shakeCoroutineReference = _instance.StartCoroutine(_instance.ShakeCoroutine());
        }

        public static void Shake(float aDuration, float aMagnitude)
        {
            _instance.shakeCoroutineReference = _instance.StartCoroutine(_instance.ShakeCoroutine(aDuration, aMagnitude));
        }

        private IEnumerator ShakeCoroutine()
        {
            IEnumerator temp = ShakeCoroutine(_instance.m_DefaultDuration, _instance.m_DefaultMagnitude);
            return temp;
        }

        private IEnumerator ShakeCoroutine(float aDuration, float aMagnitude)
        {
            Vector3 originalPosition = (localPosition ? transform.localPosition : transform.position);

            float timeElapsed = 0f;
            while (timeElapsed < aDuration)
            {
                float xOffset = Random.Range(-1f, 1f) * aMagnitude;
                float yOffset = Random.Range(-1f, 1f) * aMagnitude;

                transform.localPosition = new Vector3(originalPosition.x + xOffset, originalPosition.y + yOffset, originalPosition.z);
                timeElapsed += Time.deltaTime;

                // Run alongside Update()
                yield return new WaitForEndOfFrame();
            }

            if (localPosition)
            {
                transform.localPosition = originalPosition;
            }
            else
            {
                transform.position = originalPosition;
            }
        }
    }
}