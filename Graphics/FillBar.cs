﻿using UnityEngine;
using UnityEngine.UI;

namespace Utility.Graphics
{
    public class FillBar : MonoBehaviour
    {
        public Slider slider;
        public Image fillArea;

        public Color maxColor;
        public Color minColor;

        public float Value
        {
            get { return slider.value; }
        }

        private void Start()
        {
            fillArea.color = maxColor;
        }

        public void SetVal(float aVal)
        {
            slider.value = aVal;
            fillArea.color = Color.Lerp(minColor, maxColor, slider.value / slider.maxValue);
        }

        public void Add(float aVal)
        {
            slider.value += aVal;
        }

        public void SetMax(float aVal)
        {
            slider.maxValue = aVal;
        }
    }
}