using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Utility.Graphics
{
    public class Fade : MonoBehaviour
    {
        [SerializeField] private bool startOnAwake;
        [SerializeField] private bool gotoNextScene;
        [SerializeField] private Image target;

        [SerializeField] private float fadeInDuration = 0f; // seconds
        [SerializeField] private float fadeOutDuration = 0f; // seconds
        [SerializeField] private Color startColor;
        [SerializeField] private Color midColor;
        [SerializeField] private Color endColor;


        private void Start()
        {
            if (startOnAwake)
            {
                FadeIn();
            }
            else
            {
                target.color = midColor;
            }
        }
        public void FadeIn()
        {
            StartCoroutine(DoFade(target, startColor, midColor, fadeInDuration));
        }

        public void FadeOut()
        {
            StartCoroutine(DoFade(target, midColor, endColor, fadeOutDuration));
        }

        public void FadeToNextScene()
        {
            FadeOut();

            if (gotoNextScene)
            {
                //StartCoroutine(GotoNextScene(fadeOutDuration));
                StartCoroutine(SceneNavigation.GotoNextScene(fadeOutDuration));
            }
        }

        private static IEnumerator DoFade(Image target, Color from, Color to, float duration)
        {
            float currentTime = 0;

            while (currentTime < duration)
            {
                currentTime += Time.unscaledDeltaTime;

                target.color = Color.Lerp(from, to, currentTime / duration);

                yield return null;
            }

            yield break;
        }

        private static IEnumerator GotoNextScene(float delay)
        {
            yield return new WaitForSeconds(delay);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}