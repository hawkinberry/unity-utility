using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Utility.FMEditor
{
    public class BuildEditorWindow : EditorWindow
    {
        [SerializeField]
        private bool incrementVersion = false;
        [SerializeField]
        private bool htmlBuild = false;
        [SerializeField]
        private bool windowsBuild = false;

        [MenuItem("Window/FM/Build Pipeline")]
        public static void ShowWindow()
        {
            GetWindow(typeof(BuildEditorWindow));
        }

        void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Auto-increment version");
            incrementVersion = EditorGUILayout.Toggle(incrementVersion);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.Label("Platforms to build:", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Windows");
            windowsBuild = EditorGUILayout.Toggle(windowsBuild);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("WebGL");
            htmlBuild = EditorGUILayout.Toggle(htmlBuild);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Build"))
            {
                if (windowsBuild)
                {
                    Build(BuildTarget.StandaloneWindows, "/" + PlayerSettings.productName + ".exe");
                }
                if (htmlBuild)
                {
                    Build(BuildTarget.WebGL, "");
                }
                ///TODO: git tag builds
                //ExecuteCommand("git tag -a v" + Application.version);
                if (incrementVersion)
                {
                    IncrementVersion();
                }
            }
        }

        private void Build(BuildTarget buildTarget, string projectFolder)
        {
            string directory = "Builds/" + buildTarget.ToString() + "-" + Application.version;
            ClearBuildFolder(directory);
            BuildPipeline.BuildPlayer(GetScenes(), directory + projectFolder, buildTarget, BuildOptions.None);
        }

        private void ClearBuildFolder(string directory)
        {
            DirectoryInfo di = new DirectoryInfo(directory);
            if (di.Exists)
            {
                foreach (var file in di.EnumerateFiles())
                {
                    file.Delete();
                }
                foreach (var dir in di.EnumerateDirectories())
                {
                    dir.Delete(true);
                }
            }
        }

        private string[] GetScenes()
        {
            List<string> scenes = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                if (scene.enabled)
                {
                    scenes.Add(scene.path);
                }
            }
            return scenes.ToArray();
        }

        private void IncrementVersion()
        {
            var versionSplit = Application.version.Split('.');
            versionSplit[2] = (int.Parse(versionSplit[2]) + 1).ToString();
            PlayerSettings.bundleVersion = string.Join(".", versionSplit);
        }

        private void ExecuteCommand(string command)
        {
            ProcessStartInfo ProcessInfo;
            Process Process;

            ProcessInfo = new ProcessStartInfo("cmd.exe", "/K " + command);
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = true;

            Process = Process.Start(ProcessInfo);
        }
    }
}