﻿using UnityEngine;

namespace Utility.Actor.Router
{
    public class RandomRouter2d : Router2d
    {
        public override Vector2 GetDirection()
        {
            Vector2 return_input = Vector2.zero;

            time += Time.deltaTime;

            // If the bot reaches the point, find a new waypoint
            float distanceToGo = Vector3.Distance(transform.position, targetWaypoint);
            //Debug.Log("Waypoint distance: " + distanceToGo);
            if (distanceToGo < m_GoalSensitivity)// && time > m_IdleTimeout)
            {
                SelectNewWaypoint();
            }
            else
            {
                //Debug.Log("Routing: " + transform.position + " => " + targetWaypoint);

                return_input.x = targetWaypoint.x - transform.position.x;
                return_input.y = targetWaypoint.y - transform.position.y;
            }

            //Debug.Log("Input = " + return_input);

            return return_input;
        }

        private void SelectNewWaypoint()
        {
            throw new System.NotImplementedException();
        }
    }
}