﻿using UnityEngine;

namespace Utility.Actor.Router
{
    // Picks a single waypoint at start and ends when it arrives
    public class SingleRouter2d : Router2d
    {
        public override Vector2 GetDirection()
        {
            Vector2 return_input = Vector2.zero;

            float distanceToGo = Vector2.Distance((Vector2)transform.position, targetWaypoint);
            if (distanceToGo > m_GoalSensitivity)
            {
                //Debug.Log("Routing: " + transform.position + " => " + targetWaypoint);

                return_input.x = targetWaypoint.x - transform.position.x;
                return_input.y = targetWaypoint.y - transform.position.y;
            }

            //Debug.Log("Input = " + return_input);

            return return_input;
        }
    }
}