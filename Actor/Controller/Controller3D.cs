using UnityEngine;

namespace Utility.Actor
{
    public class Controller3D : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidBody;
        [SerializeField] protected float maxSpeed;
        private Vector3 velocity;

        protected void SetVelocity(Vector3 direction)
        {
            velocity = direction * maxSpeed;
        }

        protected virtual void FixedUpdate()
        {
            Move();
        }

        protected void Move()
        {
            rigidBody.AddForce(velocity);
        }
    }
}
