using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Utility.Menus
{
    [RequireComponent(typeof(Canvas))]
    public abstract class BaseMenu : MonoBehaviour
    {
        protected Canvas m_Overlay;

        [SerializeField]
        Selectable selectOnLoad;

        protected virtual void Awake()
        {
            m_Overlay = GetComponent<Canvas>();
            Initialize();
        }

        public abstract void Initialize();

        public bool IsVisible()
        {
            return m_Overlay.enabled;
        }

        public virtual void Enable()
        {
            gameObject.SetActive(true);
        }

        public virtual void Disable()
        {
            gameObject.SetActive(false);
        }

        // Start is called before the first frame update
        public virtual void Hide()
        {
            //m_Overlay.enabled = false;
            SetEnabled(false);
        }

        public virtual void Show()
        {
            SetEnabled(true);
            selectOnLoad?.Select();
        }

        public virtual void Toggle(bool setPause = false)
        {
            // UI is currently shown
            if (m_Overlay.enabled)
            {
                if (setPause)
                {
                    Executive.SetPause(false);
                }
                Hide();
            }
            else // UI is not shown
            {
                if (setPause)
                {
                    Executive.SetPause(true);
                }
                Show();
            }
        }

        public void SetEnabled(bool setting)
        {
            m_Overlay.enabled = setting;
        }

        public static IEnumerator FadeScreen(BaseMenu menu, Image background, Color startColor, Color endColor, float duration)
        {
            float currentTime = 0;
            float decay = duration;

            while (currentTime < decay)
            {
                currentTime += Time.unscaledDeltaTime;

                background.color = Color.Lerp(startColor, endColor, currentTime / decay);

                yield return null;
            }

            menu.Show();

            yield break;
        }
    }
}