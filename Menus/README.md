# Utility: Menus
User Interface (UI) is a critical component. It provides the means of your player to understand and interact with your game.

This menu system defines a simple process flow for enabling specific Unity UI Canvases at the right time in the game loop as defined by the Executive.

It allows you, the user, to focus on building interfaces unique to your game's style without having to implement the adminstrative portions again and again.

## Base Menu
All menu types in this system inherit from the BaseMenu class, which is itself a MonoBehaviour.

BaseMenu is also an **abstract** class, and all child classes must define the following:
* Initialize (called on BaseMenu.Awake) - e.g., does your menu Show or Hide on Awake?

BaseMenu defines the following public functions:
* **Enable** / **Disable** (Set the parent Game Object active or inactive. All MonoBehaviours cease operation.)
* **Hide** / **Show** (Enable or Disable the Canvas only. All MonoBehaviours continue operation.)

## Game Overlay
The GameOverlay class is responsible for activating defined menus, either by event triggers or by button presses.

### Usage
This should be added as a script component to your root game Canvas. It has references to other menu types provided by this system, as well as some other common game input controls (such as mute and settings keys).

| Event Subscriptions | Action |
| ------------------- | ------ |
| Executive.OnGameOver | >> EndMenu.Show()  |
| Executive.OnGameResume | >> PauseMenu.Hide() and SettingsMenu.Hide()   |
| Executive.OnGamePause | >> Hide help text  |

| Event Invocations | Trigger |
| ------------------- | ------ |
| Executive.OnGameStart | << StartGame()    |

**OnGameOver:** This should be invoked by the user _after_ Executive.OnWinGame or Executive.OnLoseGame. Note that because the action for this trigger is to display the end menu, all setup is complete by the time the menu is shown.

**OnGameResume:** If the game is resumed from pause by some other means than what the PauseMenu monitors.

**OnGamePause:** The user may wish to show some help text (e.g., "Press 'P' to pause") that is hidden once the user pauses the game.

## Game Menus
The following specific menu types are pre-defined by this system.

Add each as a script component to the GameObject containing your Canvas.

---

### Start Menu
| Event Subscriptions | Action |
| ------------------- | ------ |
| Executive.OnGameStart | >> Hide this menu    |

#### Purpose
This menu type should be displayed at the start of the game scene (for example, a menu that provides a brief introduction to your game).

---

### Pause Menu
| Event Subscriptions | Action |
| ------------------- | ------ |
| Executive.OnGameStart | >> Bypass 'first press' reaction (see below)  |

#### Purpose
This menu will be displayed when the pause input key is pressed.

#### Activation
This menu monitors the Input system for the "Pause" input.
```csharp
//! Note, this is the old Unity Input System
Input.GetButtonDown("Pause")
```

The menu passes the pause setting through to Executive.SetPause, which performs the appropriate executive action.

**On the first press:** if the Pause Menu has not already been activated, Executive.OnGameStart is invoked. This is a backup for the pause menu being enabled while the start menu is still active.

---

### Settings Menu
| Event Subscriptions | Action |
| ------------------- | ------ |
| Executive.OnGameEnd | >> Hide this menu    |

| Event Invocations | Trigger |
| ------------------- | ------ |
| Executive.OnMenuOpen | << Show()    |
| Executive.OnMenuClose | << Hide()    |

#### Purpose
This menu supports volume, music, and sound effects slider UIs. The AudioManager Set*Volume functions are added as listeners to the respective slider onValueChanged event.

#### Activation
The Settings Menu can be activated by adding **Show/Hide** (alternatively, BaseMenu.Toggle()) as an event call for a button press.

It also accepts an InputAction reference for a settings "hotkey".

Because this menu is expected to be opened during gameplay, it will notify all subscriptions when it does so.

---

### End Screen
| Event Subscriptions | Action |
| ------------------- | ------ |
| Executive.OnWinGame | >> Show "Win" Canvas parts   |
| Executive.OnLoseGame | >> Show "Lose" Canvas parts   |

#### Purpose
This menu is displayed when gameplay ends.

#### Activation
Once enabled, the End Screen will display the provided list of Game Objects according to the condition reached (win or lose).

Because this menu is expected to be opened during gameplay, it will notify all subscriptions when it does so.