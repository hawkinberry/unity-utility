using UnityEngine;

namespace Utility.Menus
{
    public class MenuLayer : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowMenu(BaseMenu menu)
        {
            menu.Show();
        }

        public void HideMenu(BaseMenu menu)
        {
            menu.Hide();
        }
    }
}