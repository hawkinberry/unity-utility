using UnityEngine;
using Utility.Achievements;

namespace Utility.Menus
{
    public class AchievementMenu : BaseMenu
    {
        [SerializeField]
        AchievementManager Manager;

        [SerializeField]
        Transform Content;

        [SerializeField]
        AchievementMenuItem LockedPrefab;

        [SerializeField]
        AchievementMenuItem UnlockedPrefab;

        public override void Initialize()
        {
            m_Overlay.enabled = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            if (Content)
            {
                Build();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void Build()
        {
            foreach (Achievement a in Manager.Achievements)
            {
                AchievementMenuItem prefab = a.IsUnlocked ? UnlockedPrefab : LockedPrefab;
                AchievementMenuItem item = Instantiate(prefab, Content);
                item.Populate(a);
            }
        }
    }
}
