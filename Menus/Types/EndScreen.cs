using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Utility.Menus
{
    public class EndScreen : BaseMenu
    {

        [SerializeField] private float fadeIn = 0f; // seconds
        [SerializeField] private Image background;
        [SerializeField] private Color startColor;
        private Color endColor;

        [SerializeField] private GameObject onWinCanvas;
        [SerializeField] private GameObject onLoseCanvas;

        [SerializeField]
        private List<GameObject> showOnLose;

        [SerializeField]
        private List<GameObject> showOnWin;

        [SerializeField]
        private PlayerInput playerInput;

        private static EndScreen m_Instance;
        private static EndScreen GetInstance() { return m_Instance; }

        public override void Initialize()
        {
            if (!m_Instance)
            {
                m_Instance = this;
            }

            endColor = background.color;
            background.color = startColor;
            m_Overlay.enabled = false;

            if (onWinCanvas) onWinCanvas.SetActive(false);
            if (onLoseCanvas) onLoseCanvas.SetActive(false);

            ShowWinObjects(false);
            ShowLoseObjects(false);
        }

        private void OnEnable()
        {
            Utility.Executive.OnWinGame += OnWin;
            Utility.Executive.OnLoseGame += OnLose;
        }

        private void OnDisable()
        {
            Utility.Executive.OnWinGame -= OnWin;
            Utility.Executive.OnLoseGame -= OnLose;
        }

        public override void Show()
        {
            base.Show();
            Executive.OnMenuOpen?.Invoke();
        }

        public override void Hide()
        {
            base.Hide();
            Executive.OnMenuClose?.Invoke();
        }

        private void OnWin()
        {
            if (onWinCanvas)
            {
                onWinCanvas.SetActive(true);
            }

            ShowWinObjects(true);
        }

        private void OnLose()
        {
            if (onLoseCanvas)
            {
                onLoseCanvas.SetActive(true);
            }

            ShowLoseObjects(true);
        }

        private void ShowWinObjects(bool enable)
        {
            foreach (GameObject o in showOnWin)
            {
                o.SetActive(enable);
            }
        }

        private void ShowLoseObjects(bool enable)
        {
            foreach (GameObject o in showOnLose)
            {
                o.SetActive(enable);
            }
        }

        public void StartTransition()
        {
            StartCoroutine(FadeScreen(this, background, startColor, endColor, fadeIn));
        }
    }
}
