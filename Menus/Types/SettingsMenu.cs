using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Utility.Menus
{
    public class SettingsMenu : BaseMenu
    {
        [SerializeField]
        Slider volumeSlider;

        [SerializeField]
        Slider musicSlider;

        [SerializeField]
        Slider sfxSlider;

        [SerializeField]
        InputActionReference settingsButton;

        private void OnEnable()
        {
            Utility.Executive.OnGameEnd += Hide;
        }

        private void OnDisable()
        {
            Utility.Executive.OnGameEnd -= Hide;
        }

        public override void Initialize()
        {
            Hide();
        }

        private void Start()
        {
            if (volumeSlider) volumeSlider.value = Audio.AudioManager.SET_VOLUME;
            if (musicSlider) musicSlider.value = Audio.AudioManager.SET_MUSIC_VOLUME;
            if (sfxSlider) sfxSlider.value = Audio.AudioManager.SET_SFX_VOLUME;

            if (volumeSlider != null)
            {
                volumeSlider.onValueChanged.AddListener(v =>
                {
                    Audio.AudioManager.SetAllVolume(volumeSlider.value);
                });
            }

            if (musicSlider != null)
            {
                musicSlider.onValueChanged.AddListener(v =>
                {
                    Audio.AudioManager.SetMusicVolume(musicSlider.value);
                });
            }

            if (sfxSlider != null)
            {
                sfxSlider.onValueChanged.AddListener(v =>
                {
                    Audio.AudioManager.SetSfxVolume(sfxSlider.value);
                });
            }
        }

        private void OnDestroy()
        {
            if (volumeSlider != null)
            {
                volumeSlider.onValueChanged.RemoveAllListeners();
            }
            if (musicSlider != null)
            {
                musicSlider.onValueChanged.RemoveAllListeners();
            }
            if (sfxSlider != null)
            {
                sfxSlider.onValueChanged.RemoveAllListeners();
            }
        }

        private void Update()
        {
            if (settingsButton && settingsButton.action.IsPressed())
            {
                Show();
            }
        }

        public override void Show()
        {
            base.Show();
            Executive.OnMenuOpen?.Invoke();
        }

        public override void Hide()
        {
            base.Hide();
            Executive.OnMenuClose?.Invoke();
        }
    }
}
