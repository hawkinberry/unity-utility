using UnityEngine;
using UnityEngine.EventSystems;

public class MenuButton : MonoBehaviour, ISelectHandler
{
    [SerializeField] AudioClip highlightSound;

    public void OnSelect(BaseEventData eventData)
    {
        Utility.Audio.AudioManager.PlaySfx(highlightSound);
    }
}