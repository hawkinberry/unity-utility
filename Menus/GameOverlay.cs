﻿using TMPro;
using UnityEngine;

namespace Utility.Menus
{
    public class GameOverlay : MonoBehaviour
    {
        [Header("Sound Controls")]
        [SerializeField] private GameObject m_SoundOnButton;
        [SerializeField] private GameObject m_SoundOffButton;
        [SerializeField] private GameObject m_MusicOnButton;
        [SerializeField] private GameObject m_MusicOffButton;
        [SerializeField] private GameObject m_SfxOnButton;
        [SerializeField] private GameObject m_SfxOffButton;

        [Header("Menus")]
        [SerializeField] private StartMenu m_StartMenu;
        [SerializeField] private PauseMenu m_PauseMenu;
        [SerializeField] private SettingsMenu m_SettingsMenu;
        [SerializeField] private EndScreen m_EndScreen;

        [Header("Text")]
        [SerializeField] private TextMeshProUGUI m_HelpText;
        private bool m_firstPause = true;

        private bool m_Enable = true;

        #region Initialize
        private void OnEnable()
        {
            // Register delegates
            Executive.OnGameOver += EndGame;
            Executive.OnGameResume += ResumeGame;
            Executive.OnGamePause += CheckPause;
        }

        private void OnDisable()
        {
            // Deregister delegates
            Executive.OnGameOver -= EndGame;
            Executive.OnGameResume -= ResumeGame;
            Executive.OnGamePause -= CheckPause;
        }

        void Start()
        {
            SoundOn();
            if (m_EndScreen) m_EndScreen.Hide();
        }

        void CheckPause()
        {
            if (m_firstPause)
            {
                m_firstPause = !m_firstPause;
                if (m_HelpText) m_HelpText.text = "";
                Executive.OnGamePause -= CheckPause;
            }
        }
        #endregion

        #region Settings
        public void OnSettings(bool setPause)
        {
            // passing setPause from OnClick isn't working.
            // always comes through as false
            m_SettingsMenu.Toggle(true);
        }

        public void OnSettingsBack(bool hidePause = false)
        {
            m_SettingsMenu.Hide();
        }
        #endregion


        public void StartGame()
        {
            Executive.OnGameStart?.Invoke();
        }

        public void ResumeGame()
        {
            if (m_PauseMenu) m_PauseMenu.Hide();
            if (m_SettingsMenu) m_SettingsMenu.Hide();
        }

        public void EndGame()
        {
            if (m_EndScreen)
            {
                m_EndScreen.Show();
                m_EndScreen.StartTransition();
            }
        }

        public void Disable()
        {
            m_Enable = false;
        }

        public void SoundOn()
        {
            MusicOn();
            SfxOn();

            if (m_SoundOffButton) m_SoundOffButton.SetActive(true);
            if (m_SoundOnButton) m_SoundOnButton.SetActive(false);
        }

        public void MusicOn()
        {
            Audio.AudioManager.SetMusicMute(false);

            if (m_MusicOffButton) m_MusicOffButton.SetActive(true);
            if (m_MusicOnButton) m_MusicOnButton.SetActive(false);
        }

        public void MusicOff()
        {
            Audio.AudioManager.SetMusicMute(true);

            if (m_MusicOffButton) m_MusicOffButton.SetActive(false);
            if (m_MusicOnButton) m_MusicOnButton.SetActive(true);
        }

        public void SfxOn()
        {
            Audio.AudioManager.SetSfxMute(false);

            if (m_SfxOffButton) m_SfxOffButton.SetActive(true);
            if (m_SfxOnButton) m_SfxOnButton.SetActive(false);
        }

        public void SfxOff()
        {
            Audio.AudioManager.SetSfxMute(true);

            if (m_SfxOffButton) m_SfxOffButton.SetActive(false);
            if (m_SfxOnButton) m_SfxOnButton.SetActive(true);
        }

        public void OnMusicButtonPressed()
        {
            bool music_muted = Audio.AudioManager.ToggleMusicMute();

            m_MusicOffButton.SetActive(!music_muted);
            m_MusicOnButton.SetActive(music_muted);
        }

        public void OnSfxButtonPressed()
        {
            bool sfx_muted = Audio.AudioManager.ToggleSfxMute();

            m_SfxOffButton.SetActive(!sfx_muted);
            m_SfxOnButton.SetActive(sfx_muted);
        }

        public void OnSoundButtonPressed()
        {
            bool muted = Audio.AudioManager.ToggleMute();

            m_SoundOffButton.SetActive(!muted);
            m_SoundOnButton.SetActive(muted);
        }
    }
}
