using System.Collections.Generic;
using UnityEngine;

namespace Utility.Achievements
{
    [CreateAssetMenu(menuName = "Achievements/Manager")]
    public class AchievementManager : ScriptableObject
    {
        public AchievementEvent OnAchievementUnlocked;

        [SerializeField]
        List<Achievement> achievements;
        private Dictionary<string, Achievement> achievementDictionary;

        public List<Achievement> Achievements
        {
            get { return achievements; }
        }

        public void Initialize()
        {
            Debug.Log("Defining achievement dictionary");
            achievementDictionary = new Dictionary<string, Achievement>();
            foreach (Achievement a in achievements)
            {
                achievementDictionary.Add(a.Id, a);
            }
        }

        public void UnlockAchievement(string id)
        {
            Achievement achievement;
            achievementDictionary.TryGetValue(id, out achievement);

            if (achievement && !achievement.IsUnlocked)
            {
                Debug.Log("Unlocked " + id + "!");
                achievement.Unlock();
                OnAchievementUnlocked?.Invoke(achievement);
            }
        }

        public void ResetProgress()
        {
            foreach (Achievement a in achievements)
            {
                a.Reset();
            }
        }

        public delegate void AchievementEvent(Achievement achievement);
    }
}