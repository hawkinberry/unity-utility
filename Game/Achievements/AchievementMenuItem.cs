﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utility.Achievements
{
    public class AchievementMenuItem : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI title;
        [SerializeField]
        TextMeshProUGUI description;
        Image image;

        public void Populate(Achievement info)
        {
            title.text = info.Title;
            description.text = info.Description;

            // TODO: Set image
            // TODO: Set unlocked
        }
    }
}
