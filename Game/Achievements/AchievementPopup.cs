using TMPro;
using UnityEngine;

namespace Utility.Achievements
{
    public class AchievementPopup : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup canvasGroup;
        [SerializeField]
        private TMP_Text achievementTitle;
        [SerializeField]
        private TMP_Text achievementDescription;
        [SerializeField]
        private float timerToShowPopup = 2;

        private float timer = 0;

        private void OnEnable()
        {
        }

        private void OnDisable()
        {
            GameManager.Instance.achievementManager.OnAchievementUnlocked -= OnAchievementUnlocked;
        }

        private void Start()
        {
            GameManager.Instance.achievementManager.OnAchievementUnlocked += OnAchievementUnlocked;
            DontDestroyOnLoad(gameObject);
        }

        private void FixedUpdate()
        {
            if (timer > 0)
            {
                timer -= Time.fixedDeltaTime;
                if (timer <= 0)
                {
                    canvasGroup.alpha = 0;
                }
            }
        }

        private void OnAchievementUnlocked(Achievement achievement)
        {
            achievementTitle.text = achievement.Title;
            achievementDescription.text = achievement.Description;
            timer = timerToShowPopup;
            canvasGroup.alpha = 1;
        }
    }
}