using UnityEditor;
using UnityEngine;

namespace Utility.Achievements
{
    [CustomEditor(typeof(AchievementManager))]
    public class AchievementManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            AchievementManager manager = (AchievementManager)target;

            if (GUILayout.Button("Reset progress"))
            {
                if (EditorUtility.DisplayDialog("Reset Achievements",
                "Are you sure you want to reset ALL achievement progress?", "Yes", "Cancel"))
                {
                    manager.ResetProgress();
                }
            }
        }
    }
}