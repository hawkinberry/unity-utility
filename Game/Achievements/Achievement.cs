using UnityEngine;

namespace Utility.Achievements
{
    [CreateAssetMenu(menuName = "Achievements/Achievement")]
    public class Achievement : ScriptableObject
    {
        [SerializeField]
        private string id;
        public string Id { get { return id; } }

        [SerializeField]
        private string title = "New Achievement";
        public string Title { get { return title; } }

        [SerializeField]
        private string description = "Enter achievement description.";
        public string Description { get { return description; } }

        [SerializeField]
        private bool unlocked = false;
        public bool IsUnlocked { get { return unlocked; } }

        public void Unlock()
        {
            unlocked = true;
        }

        public void Reset()
        {
            unlocked = false;
        }
    }
}