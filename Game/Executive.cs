﻿using UnityEngine;

namespace Utility
{
    public static class Executive
    {
        public enum GAME_STATE
        {
            INIT = 0,
            PLAY,
            PAUSE,
            OVER
        };

        /*
         * Game Events are to be used to pass game-loop events between game objects
         * and the game executor, which should be a bespoke class specific to the game.
         * The following describes intended best practices in using these events in your game.
         */
        /*
         * OnGameStart should be invoked by the game executor once all setup is complete.
         * Anything that should wait to start can subscribe to this event.
         */
        public static GameExecutiveHandler OnGameStart;
        /*
         * OnGamePause/Resume is invoked by the Executive when SetPause is called.
         * Anything that should respond to game pause state changes should subscribe to these.
         */
        public static GameExecutiveHandler OnGamePause;
        public static GameExecutiveHandler OnGameResume;
        /*
         * OnMenuOpen/Close are invoked by the BaseMenu class to allow handling of
         * input scheme changes.
         */
        public static GameExecutiveHandler OnMenuOpen;
        public static GameExecutiveHandler OnMenuClose;
        /*
         * On Win/Lose Game should be invoked when the win/lose condition occurs.
         * Typically, the main game executor will decide what happens on that trigger.
         */
        public static GameExecutiveHandler OnWinGame;
        public static GameExecutiveHandler OnLoseGame;
        /*
         * OnGameOver should be triggered by the game executor once any setup
         * corresponding to the win/lose condition has been handled.
         * Anything that relates to communicating the game state to the play should subscribe here.
         */
        public static GameExecutiveHandler OnGameOver;
        /*
         * OnGameEnd is invoked by the game executive once the game should stop.
         * Anything that needs to suspend gametime operations should subscribe to this.
         */
        public static GameExecutiveHandler OnGameEnd;

        public static float TimeScale = Time.timeScale;
        public static GAME_STATE GameState;

        //! This function will set the Unity TimeScale to 0, which will also pause all animations.
        //! param paused whether to pause (true) or unpause (false) the game
        //! OnGamePause and OnGameResume are invoked
        public static void SetPause(bool paused)
        {
            Time.timeScale = (paused ? 0f : 1f);
            GameState = (paused ? GAME_STATE.PAUSE : GAME_STATE.PLAY);

            //Audio.AudioManager.SetPause(paused);

            if (paused)
            {
                Debug.Log("Pausing!");
                OnGamePause?.Invoke();
            }
            else
            {
                OnGameResume?.Invoke();
            }
        }

        public static bool IsPaused()
        {
            return GameState == GAME_STATE.PAUSE;
        }

        //! Sets GameState = OVER
        //! OnGameEnd is invoked
        public static void EndGame()
        {
            GameState = GAME_STATE.OVER;
            OnGameEnd?.Invoke();
        }

        public static bool IsGameOver()
        {
            return GameState == GAME_STATE.OVER;
        }

        // Game Delegates
        public delegate void GameExecutiveHandler();
    }
}
