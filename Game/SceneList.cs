using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    [CreateAssetMenu(menuName = "Utility/SceneList")]
    public class SceneList : ScriptableObject
    {
        [SerializeField]
        public string MainMenuScene = "MainMenu";
        [SerializeField]
        public string CreditsScene = "Credits";
        [SerializeField]
        public string GameScene = "GameScene";
    }
}
