﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utility
{
    public class SceneNavigation : MonoBehaviour
    {
        private static SceneNavigation m_Instance;

        public static SceneNavigation GetInstance() { return m_Instance; }

        [SerializeField]
        SceneList sceneList;

        private void Awake()
        {
            m_Instance = this;
        }

        //! Loads sceneList.MainMenuScene
        public void OnMainMenuButtonPressed()
        {
            // In case we are coming here from a pause menu
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneList.MainMenuScene);
        }

        public static IEnumerator GotoNextScene(float delay = 0)
        {
            yield return new WaitForSeconds(delay);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //! Loads sceneList.GameScene
        //! Sets TimeScale to 1
        public void OnStartGame()
        {
            // Make sure we load in a clean state
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneList.GameScene);
        }

        //! Loads sceneList.CreditScene
        public void OnCreditsButtonPressed()
        {
            // In case we are coming here from a pause menu
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneList.CreditsScene);
            OnButtonPress();
        }

        public static void OnButtonPress()
        {
            Audio.AudioManager.PlaySound("button");
        }

        //! Quits the game application (or stops the player in Editor mode)
        public void OnQuitButtonPressed()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }

        public void LoadLevel(string levelName)
        {
            SceneManager.LoadScene(levelName);
        }
    }
}
