using UnityEngine;

namespace Utility
{
    using Achievements;

    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;
        public static GameManager Instance { get { return _instance; } }

        [SerializeField]
        public AchievementManager achievementManager;

        [SerializeField]
        TMPro.TextMeshProUGUI versionText;


        // Start is called before the first frame update
        void OnEnable()
        {
            if (!_instance)
            {
                _instance = this;
            }

            if (achievementManager)
            {
                achievementManager.Initialize();
            }

            if (versionText)
            {
                versionText.text += Application.version;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}