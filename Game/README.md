# Utility: Game Management
The Executive, SceneNavigation, and GameManager classes provide a foundation for building a game loop (start, play, end, restart).

## Game Executive
The Executive is a static class that defines game state and its transitions at a fundamental level. While the Executive does not itself execute any game logic (it is not a MonoBehaviour), it is more of a nexus through which user-defined conditions or triggers can be mapped to one another.

### Game State
The Executive exposes a record of high-level state (whether the game is playing, paused, or complete). The main use case for this value is for game elements that wish to modify their operation in different states.

```csharp
public static GAME_STATE GameState;

public enum GAME_STATE
{
    INIT = 0,
    PLAY,
    PAUSE,
    OVER
};
```

The GameState value will change between `PLAY` and `PAUSE` when the following function is called.
```csharp
//! Note: this function will set the Unity TimeScale to 0, which will also pause all animations.
//! param paused whether to pause (true) or unpause (false) the game
//! OnGamePause and OnGameResume are invoked
public static void SetPause(bool paused)
```

The user should call this function when the gameplay has completed (i.e., the player has won or lost).
```csharp
//! Sets GameState = OVER
//! OnGameEnd is invoked
public static void EndGame()
```

#### Implementation Guidance
Wrapping modes in conditionals ("is the game playing or not?") is typically not the best design. It leads to code that can be difficult to maintain. Instead, it is recommended to use the existing GameObject life cycle (https://docs.unity3d.com/Manual/ExecutionOrder.html) to deactivate elements of your game.

That said, there is a time and a place for imperfect design (especially in prototypes). For example, an agent that performs a periodic act while the game is running may instead pause this act while the game is over without necessarily removing the agent from the scene altogether.

This pattern allows for flexibility when defining what these executive states mean in your game (for example, just because a game is over doesn't mean that everything ceases operation).

### Game Events
The Executive defines crucial events that signal transitions in the game state. These events are mainly used by the Menu system to properly handle UI transitions, though they can also be subscribed when a specific response within a user-defined GameObject is desired.

It is expected that events are fired in this order:
    
    OnGameStart >> OnWinGame/OnLoseGame (Strictly One) >> OnGameOver >> OnGameEnd

**Implementation:** Your game should define an "executor"--that is, a Monobehaviour that is responsible for invoking these events (as well as defining their satisfying conditions).

The following contains usage guidance.

```csharp
// Invoked by:      Menus.GameOverlay
// Registered by:   Menus.StartMenu, Menus.PauseMenu
public static GameExecutiveHandler OnGameStart;

// Invoked by:      Executive.SetPause
// Registered by:   Menus.GameOverlay
public static GameExecutiveHandler OnGamePause;
public static GameExecutiveHandler OnGameResume;

// Invoked by:      Menus.SettingsMenu
// Registered by:   User (anything that needs to respond to a UI change)
public static GameExecutiveHandler OnMenuOpen;
public static GameExecutiveHandler OnMenuClose;

// Invoked by:      User (Defined by Win/Lose condition)
// Registered by:   Menus.EndScreen
public static GameExecutiveHandler OnWinGame;
public static GameExecutiveHandler OnLoseGame;
        
// Invoked by:      User (when gameplay after win/loss is complete)
// Registered by:   Menus.GameOverlay
public static GameExecutiveHandler OnGameOver;

// Invoked by:      Executive.EndGame (should be called by user)
// Registered by:   Menus.SettingsMenu, User (anything that needs to suspend gameplay operations)
public static GameExecutiveHandler OnGameEnd;
```
## Scene Navigation
The SceneNavigation class defines functions that invoke the Unity SceneManager using a SceneList Scriptable Object reference, which contains the names of user-defined game scenes. 

```csharp
public class SceneList : ScriptableObject
{
    public string MainMenuScene;
    public string GameScene;
    public string CreditsScene;
}
```

These functions are intended to be tied to events (e.g., a button press). SceneNavigation also implements the Singleton pattern, so that the following functionality can be called by instance reference or static reference.
```csharp
public class SceneNavigation : MonoBehaviour
{
    private static SceneNavigation m_Instance;

    SceneList sceneList;

    //! Loads sceneList.MainMenuScene
    public void OnMainMenuButtonPressed() 
    
    //! Loads sceneList.GameScene
    public void OnStartGame()

    //! Loads sceneList.CreditScene
    public void OnCreditsButtonPressed()

    //! Quits the game application (or stops the player in Editor mode)
    public void OnQuitButtonPressed()
}
```

## Game Manager
The GameManager is a newer class that has minimal functionality. It is a Monobehaviour that performs the following actions on enable:
* Initializes the AchievementManager (assuming one is instantiated)
* Populates a version UI string from UnityEngine.Application.version