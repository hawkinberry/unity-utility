# organization
To install, add this repository (as a submodule or subtree, or as a separate copy if you wish) into a folder of your choosing within your Unity project (for example, Assets/Scripts/unity-utility).

This repository was last updated for Unity Editor Version `2022.3.23f1`

# unity-utility
**Purpose:** This software package for the Unity Engine accelerates the delivery of prototypes by offering a set of core administrative game functions that complete the game loop. 

The package also collects various routines that are commonly used (e.g., actor controls or graphics manipulations), so that simple functionality need not be rewritten in each project.

**Vision:** This package is expected to mature with each inclusion, as new edge cases are encountered. It will also evolve as new uses cases are discovered. 

**Explore each of these folders for more information.**

## Game
These scripts manage and emit alerts for changes to game state. There is also a simple scene navigation component, as well as a framework for integrating local achievements into your game.
## Menus
These scripts provide a simple yet functional management of in-game menus, allowing user-defined User Interfaces (UIs) to be enabled and disabled according to user-defined triggers.
## Audio
The AudioManager centralizes the emission of music and sound effects for simple audio designs. For more complex designs requiring any number of audio sources, this singleton pattern also manages volume levels for all registered sources.