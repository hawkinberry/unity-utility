# Utility: Audio Manager
The AudioManager class is a Singleton. Only one instance should be included in each scene. Using its functionality properly ensures that resources are cleaned as intended when scenes are unloaded.

The AudioManager provides two main interfaces for games that wish to implement sound.

1. Simple calls to direct user-defined sound clips through default audio sources.
2. Management of user-registered audio sources.

***Design note:** At this point, the AudioManager script file is quite large and could be broken into subclasses which are wrapped by the publically visible AudioManager singleton, for readability.*

## Playing Audio Clips through default Audio Sources
Games that wish to implement sound (music or sound effects) need not have a complicated infrastructure. However, implementing audio within each project can suffer from a few challenges: dragging a single reference across many components (which is a pain should that reference change) or from managing the settings of multiple sources scattered throughout game objects.

Instead, the AudioManage offers **static access** to singular music and sound effects sources. Components that define specific Audio Clips may simply request that these be played by invoking the following functions.

```csharp
public static void PlayMusic(AudioClip clip) // see source for additional parameters
public static void PlaySfx(AudioClip clip) // see source for additional parameters
```

## Managing Audio Levels
The benefit of centralizing the playing of audio clips through default sources is that it is much easier to manage the customization of these sources by configuration (or even programatically) in a single place.

For example, the AudioManager accepts volume settings (master, sound effects, music) so that all requested clips are played at the correct balance.
```csharp
// Public getters for actual sound balance (e.g., setting * master setting)
// Use these when playing an audio clip!
public static float VOLUME;
public static float VOLUME_SFX;
public static float VOLUME_MUSIC;
```


### Changing Volume Settings
The following routines are provided to change the current volume setting. These can be registered to listeners for certain events, such as changing a slider input, and the AudioManager will automatically adjust all sources.
```csharp
public static void SetAllVolume(float input)
public static void SetSfxVolume(float input)
public static void SetMusicVolume(float input)
```


## Registered Audio Sources
A game's audio implementation might require more than the default audio sources (for example, sources that are components on instantiated Prefabs). The AudioManager can also manage the volume of these that are properly registered through the following routines.

### Implementation:
The recommended pattern is to call the Register and Unregister routines in a Monobehavior's OnEnable and OnDisable callbacks. This ensures that destroyed AudioSources are not still operated on when they go out of scope.

```csharp
public class MyScript : MonoBehavior
{
    // ...

    void OnEnable()
    {
        Utility.Audio.AudioManager.RegisterSource(ref mySource);
    }

    void OnDisable()
    {
        Utility.Audio.AudioManager.UnregisterSource(ref mySource);
    }
}
```

### Registration Calls:
```csharp
// Register and Unregister an AudioSource reference to manage its audio
public static void RegisterMusicSource(ref AudioSource aSource)
public static void UnregisterMusicSource(ref AudioSource aSource)
public static void RegisterSFXSource(ref AudioSource aSource)
public static void UnregisterSFXSource(ref AudioSource aSource)

// Unspecified sources will be managed at the master "volume" level
public static void RegisterSource(ref AudioSource aSource)
public static void UnregisterSource(ref AudioSource aSource)
```

## Audio Effects
### Fade Music
```csharp
public static IEnumerator StartFade(
    AudioSource audioSource, 
    float duration, 
    float targetVolume
    )
```

To implement the above routine, use the following syntax.
```csharp
UnityEngine.StartCoroutine(
    Utility.Audio.AudioManager.StartFade(
        m_MusicPlayer, 10f, Utility.Audio.AudioManager.VOLUME_MUSIC
        )
    );
```

### Play Random Sound
The following routines are very useful for avoiding monotonous or repetitive sound clips, as they each choose a clip at random (using UnityEngine.Random);
```csharp
//! Play a random sound from the list through the default SFX source
public static AudioClip PlayRandomSound(
    ref List<AudioClip> aList, 
    float aVolume
    )

//! Play a random sound from the list through the provided source
public static AudioClip PlayRandomSound(
    ref AudioSource aSource, 
    ref List<AudioClip> aList, 
    float aVolume
    )
```