﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Audio
{
    //[RequireComponent(typeof(AudioSource))]
    // Based on https://www.youtube.com/watch?v=8pFlnyfRfRc
    public class AudioManager : MonoBehaviour
    {
        [SerializeField]
        private AudioClip buttonClick;

        static bool m_Muted = false;

        public static bool IsMuted { get { return m_Muted; } }

        static bool m_MusicMuted = false;
        static bool m_MusicPaused = false;
        public static bool IsMusicMuted { get { return m_MusicMuted; } }

        static bool m_SfxMuted = false;
        static bool m_SfxPaused = false;
        public static bool IsSfxMuted { get { return m_SfxMuted; } }

        public static AudioManager m_Instance;

        // Audio clip collections
        static List<AudioClip> m_sounds = new List<AudioClip>();

        // Music
        public static int mLastMusicPosition;

        // Audio sources
        private static AudioSource m_SfxSource;
        private static AudioSource m_MusicPlayer;

        // Audio source collections
        private static List<AudioSource> m_Sources = new List<AudioSource>();
        private static Dictionary<AudioSource, float> m_SfxSources = new Dictionary<AudioSource, float>();
        private static Dictionary<AudioSource, float> m_MusicSources = new Dictionary<AudioSource, float>();

        // Configurable volume settings
        private static float VOLUME_SETTING = 0.5f;
        private static float SFX_VOLUME_SETTING = 1f;
        private static float MUSIC_VOLUME_SETTING = 1f;

        // Public setters for absolute values
        public static float SET_VOLUME { get { return VOLUME_SETTING; } set { VOLUME_SETTING = value; } }
        public static float SET_SFX_VOLUME { get { return SFX_VOLUME_SETTING; } set { SFX_VOLUME_SETTING = value; } }
        public static float SET_MUSIC_VOLUME { get { return MUSIC_VOLUME_SETTING; } set { MUSIC_VOLUME_SETTING = value; } }

        // Public getters for actual sound balance
        public static float VOLUME { get { return VOLUME_SETTING; } }
        public static float VOLUME_SFX { get { return VOLUME_SETTING * SFX_VOLUME_SETTING; } }
        public static float VOLUME_MUSIC { get { return VOLUME_SETTING * MUSIC_VOLUME_SETTING; } }

        public static AudioSource MusicSource
        {
            get { return m_MusicPlayer; }
        }

        public static AudioSource SfxSource
        {
            get { return m_SfxSource; }
        }

        public static void RegisterSource(ref AudioSource aSource)
        {
            if (!m_Sources.Contains(aSource)) m_Sources.Add(aSource);
            aSource.volume = VOLUME;
        }

        public static void UnregisterSource(ref AudioSource aSource)
        {
            m_Sources.Remove(aSource);
        }

        public static void RegisterMusicSource(ref AudioSource aSource)
        {
            if (!m_MusicSources.ContainsKey(aSource)) m_MusicSources.Add(aSource, aSource.volume);
            if (!m_Sources.Contains(aSource)) m_Sources.Add(aSource);
            aSource.volume *= VOLUME_MUSIC;
        }

        public static void UnregisterMusicSource(ref AudioSource aSource)
        {
            m_MusicSources.Remove(aSource);
            m_Sources.Remove(aSource);
        }

        public static void RegisterSFXSource(ref AudioSource aSource)
        {
            if (!m_SfxSources.ContainsKey(aSource)) m_SfxSources.Add(aSource, aSource.volume);
            if (!m_Sources.Contains(aSource)) m_Sources.Add(aSource);
            aSource.volume *= VOLUME_SFX;
        }

        public static void UnregisterSFXSource(ref AudioSource aSource)
        {
            m_SfxSources.Remove(aSource);
            m_Sources.Remove(aSource);
        }

        private static void SetVolume(ref Dictionary<AudioSource, float> rSources, float aSetting)
        {
            foreach (AudioSource src in rSources.Keys)
            {
                if (src.mute == false)
                {
                    src.volume = aSetting * rSources[src];
                }
            }
        }

        public static void SetAllVolume(float input)
        {
            VOLUME_SETTING = input;
            // Adjust sources but do not modify current settings
            SetVolume(ref m_SfxSources, VOLUME_SFX);
            SetVolume(ref m_MusicSources, VOLUME_MUSIC);
        }

        public static void SetSfxVolume(float input)
        {
            SFX_VOLUME_SETTING = input;
            SetVolume(ref m_SfxSources, VOLUME_SFX);
        }

        public static void SetMusicVolume(float input)
        {
            if (fadeMusicCoroutine != null)
            {
                m_Instance.StopCoroutine(fadeMusicCoroutine); // in case we are still fading music track in
            }

            MUSIC_VOLUME_SETTING = input;
            SetVolume(ref m_MusicSources, VOLUME_MUSIC);
        }

        void Awake()
        {
            m_SfxSources.Clear();
            m_MusicSources.Clear();

            if (!m_Instance)
            {
                m_Instance = this;
            }

            m_SfxSource = gameObject.transform.Find("SFX").GetComponent<AudioSource>();
            m_MusicPlayer = gameObject.transform.Find("Music").GetComponent<AudioSource>();

            RegisterSFXSource(ref m_SfxSource);
            RegisterMusicSource(ref m_MusicPlayer);
        }

        // Start is called before the first frame update
        void Start()
        {
            // In case we reloaded from pause menu
            SetPause(false);

            m_MusicPlayer.enabled = true;
            if (m_MusicPlayer.clip)
            {
                //SetMusicVolume(0); // start off and fade in
                //SetVolume(ref m_MusicSources, 0f);
                m_MusicPlayer.Play();
                //fadeMusicCoroutine = StartCoroutine(StartFade(m_MusicPlayer, 10f, VOLUME_MUSIC));
            }
            else
            {
                //SetMusicVolume(VOLUME_MUSIC);
            }
        }

        private static Coroutine fadeMusicCoroutine;

        // See https://gamedevbeginner.com/how-to-fade-audio-in-unity-i-tested-every-method-this-ones-the-best/
        public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
        {
            if (fadeMusicCoroutine != null)
            {
                m_Instance.StopCoroutine(fadeMusicCoroutine); // in case we are still fading music track in
            }

            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.unscaledDeltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
            yield break;
        }

        public static bool ToggleMusicMute()
        {
            SetMusicMute(!m_MusicMuted);
            return m_MusicMuted;
        }

        public static void SetMusicMute(bool setting)
        {
            m_MusicMuted = setting;

            foreach (AudioSource s in m_MusicSources.Keys)
            {
                s.mute = m_MusicMuted;
            }
        }

        public static bool ToggleSfxMute()
        {
            SetSfxMute(!m_SfxMuted);
            return m_SfxMuted;
        }

        public static void SetSfxMute(bool setting)
        {
            m_SfxMuted = setting;

            foreach (AudioSource s in m_SfxSources.Keys)
            {
                s.mute = m_SfxMuted;
            }
        }

        public static bool ToggleMute()
        {
            SetMute(!m_Muted);
            return m_Muted;
        }

        public static void SetMute(bool setting)
        {
            m_Muted = setting;

            // Toggle sfx sources, but preserve SFX mute setting
            foreach (AudioSource s in m_SfxSources.Keys)
            {
                s.mute = m_SfxMuted || m_Muted;
            }

            // Toggle sfx sources, but preserve music mute setting
            foreach (AudioSource s in m_MusicSources.Keys)
            {
                s.mute = m_MusicMuted || m_Muted;
            }

            // cleanup remaining sources
            foreach (AudioSource s in m_Sources)
            {
                if (!m_SfxSources.ContainsKey(s) && !m_MusicSources.ContainsKey(s))
                {
                    s.mute = m_Muted;
                }
            }
        }

        public static void SetPause(bool pause)
        {
            m_SfxPaused = pause;
            m_MusicPaused = pause;
            foreach (AudioSource s in m_Sources)
            {
                if (s != null)
                {
                    if (pause)
                    {
                        s.Pause();
                    }
                    else
                    {
                        s.UnPause();
                    }
                }
            }
        }

        //! Plays the provided clip at the current SFX volume through the default sound effects Audio Source
        //! param clip the Audio Clip to play
        //! param (optional) loop whether to play the clip as looping
        public static void PlaySfx(AudioClip clip, bool loop = false)
        {
            if (m_SfxPaused)
            {
                // Play calls will force current source to un-pause
                return;
            }

            if (clip)
            {
                if (loop)
                {
                    m_SfxSource.clip = clip;
                    m_SfxSource.Play();
                }
                else
                {
                    m_SfxSource.PlayOneShot(clip, VOLUME_SFX);
                }
            }
            /*else
            {
                Debug.Log("Cannot play null sfx clip.");
            }*/
        }

        //! Stop the default SFX source
        public static void StopSfx()
        {
            m_SfxSource.Stop();
        }

        //! Stop all registered SFX sources
        public static void StopAllSfx()
        {
            foreach (AudioSource s in m_SfxSources.Keys)
            {
                s.Stop();
            }
        }

        //! Plays the provided clip through the default music Audio Source
        //! param clip the Audio Clip to play
        //! param (optional) loop whether to play the clip as looping
        //! param (optional) resume whether to start the clip at the last recorded sample (mLastMusicPosition)
        public static void PlayMusic(AudioClip clip, bool loop = false, bool resume = false)
        {
            if (m_MusicPaused)
            {
                // Play calls will force current source to un-pause
                return;
            }

            if (clip)
            {
                if (loop)
                {
                    m_MusicPlayer.clip = clip;
                    if (resume)
                    {
                        m_MusicPlayer.timeSamples = mLastMusicPosition;
                    }
                    m_MusicPlayer.Play();
                }
                else
                {
                    m_MusicPlayer.PlayOneShot(clip);
                }
            }
            /*else
            {
                Debug.Log("Cannot play null music clip.");
            }*/
        }

        //! Start the default music source
        //! param (optional) resume from last sample (mLastMusicPosition)
        public static void StartMusic(bool resume = false)
        {
            if (resume)
            {
                m_MusicPlayer.timeSamples = mLastMusicPosition;
            }

            m_MusicPlayer.Play();
        }

        //! Stop the default music source
        //! param (optional) resume to remember the current sample (mLastMusicPosition)
        public static void StopMusic(bool resume = false)
        {
            if (resume)
            {
                mLastMusicPosition = m_MusicPlayer.timeSamples;
            }

            m_MusicPlayer.Stop();
        }

        public static bool IsMusicPlaying()
        {
            return m_MusicPlayer.isPlaying;
        }

        public static void PlaySound(string clip)
        {
            switch (clip)
            {
                case "button":
                    PlayButtonPress();
                    break;
                default:
                    //Debug.Log("Invalid audio clip " + clip + " requested");
                    break;
            }
        }

        public static AudioClip PlayRandomSound(ref AudioSource aSource, ref List<AudioClip> aList, float aVolume)
        {
            if (aList.Count == 0) return null;

            int choice = Random.Range(0, aList.Count);
            aSource.PlayOneShot(aList[choice], aVolume);

            return aList[choice];
        }

        public static AudioClip PlayRandomSound(ref List<AudioClip> aList, float aVolume)
        {
            return PlayRandomSound(ref m_SfxSource, ref aList, aVolume);
        }

        private static void PlayButtonPress()
        {
            if (m_Instance)
            {
                m_SfxSource.PlayOneShot(m_Instance.buttonClick, VOLUME_SFX);
            }
        }

    }
}
